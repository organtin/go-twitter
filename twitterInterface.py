############################################################################                                   
#                                                                                                              
#    twitterInterface.py - a python script to get your twitter timeline
#                  and to post a tweet             
#                  copyright (c) by giovanni.organtini@roma1.infn.it 2015          
#                                                                                                              
#    This program is free software: you can redistribute it and/or modify                                      
#    it under the terms of the GNU General Public License as published by                                      
#    the Free Software Foundation, either version 3 of the License, or                                         
#    (at your option) any later version.                                                                       
#                                                                                                              
#    This program is distributed in the hope that it will be useful,                                           
#    but WITHOUT ANY WARRANTY; without even the implied warranty of                                            
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                             
#    GNU General Public License for more details.                                                              
#                                                                                                              
#    You should have received a copy of the GNU General Public License                                         
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.                                     
#                                                                                                              
############################################################################                                   

import os
import sys
import time
import hmac
from hashlib import sha1
import urllib
import subprocess
import shlex
import json
import pprint

#
# This class reads the configuration file with tokens
#
class FileReader:
    dictionary = {}
    def __init__(self):
        with open('.secrets') as f:
            content = f.readlines()
        for line in content:
            keyvalue = line.rstrip('\n').split('=')
            n = len(keyvalue);
            if (n == 2):
                key = keyvalue[0].strip(' ')
                value = keyvalue[1].strip(' ')
                self.dictionary[key] = value
    def get(self, key):
        # return the required token
        return self.dictionary[key]
    def urlencoded(self, key):
        # return the required token as an urlencoded string
        return urllib.quote_plus(self.get(key))


#
# This class provides OAuth authentication with Twitter API
#
class OAuth:
    basetstr = ""
    # get the secret tokens
    fr = FileReader() 
    timestamp = ""
    nonce = ""
    def __init__(self):
        # constructor: get the timestamp and nonce as an urlencoded base64 string
        self.timestamp = str(int(time.time()))
        self.nonce = urllib.quote_plus(self.timestamp.encode("base64").rstrip('\n'))
    def header(self, method, api_url, *args):
        # return the header of the request
        parameters = ''
        headstring = 'Authorization: OAuth oauth_consumer_key="' + \
                     self.fr.urlencoded('CONSUMERKEY') + '", oauth_nonce="' + \
                     self.nonce + '", oauth_signature_method="HMAC-SHA1", oauth_token="' + \
                     self.fr.urlencoded('ACCESSTOKEN') + '", oauth_timestamp="' + self.timestamp + \
                     '", oauth_version="1.0", oauth_signature="' + \
                     self.signature(method, api_url, *args) + '"'
        return headstring
    def parameter_string(self):
        # return the parameter string of the request
        ret = 'oauth_consumer_key=' + self.fr.get('CONSUMERKEY') + \
              '&oauth_nonce=' + self.nonce + \
                              '&oauth_signature_method=HMAC-SHA1&oauth_timestamp=' + \
                              self.timestamp + '&oauth_token=' + self.fr.get('ACCESSTOKEN') + \
                                                               '&oauth_version=1.0'
        return ret
    def basestr(self, method, api_url):
        # return the basestring to be encoded to provide the signature
        ret = method + '&' + urllib.quote_plus(api_url) + '&' + \
              urllib.quote_plus(self.parameter_string())
        return ret
    def signature(self, method, api_url, *args):
        # encode a basestring into an urlencoded base 64 string, once signed using sha1 algorithm
        signaturebasestr = self.basestr(method, api_url)
        if (len(args) > 0):
            if 'update' in api_url:
                signaturebasestr += urllib.quote('&status=' + urllib.quote(args[0])) 
            if ('direct_messages' in api_url) and (len(args) > 0):
                signaturebasestr += urllib.quote('&screen_name=' + args[1] + \
                                                 '&text=' + urllib.quote(args[0]))
        key = self.fr.get('CONSUMERSECRET') + "&" + self.fr.get('ACCESSTOKENSECRET')
        hashed = hmac.new(key, signaturebasestr, sha1)
        encoded = urllib.quote_plus(hashed.digest().encode("base64").rstrip('\n'))
        return encoded
    def curlCommand(self, method, api_url, *args):
        # return the curl command string for the given method and url
        tweet = ''
        data = ''
        screen_name = ''
        if 'update' in api_url:
            tweet = args[0]
            data = " --data 'status=" + urllib.quote(tweet) + "'"
        if ('direct_messages' in api_url) and (len(args) > 0):
            tweet = args[0]
            screen_name = args[1]
            data = " --data 'screen_name=" + screen_name + "&text=" + urllib.quote(tweet) +"'"
        ccmd = "curl -k --request '" + method + "' '" + api_url + "'" + data + " --header '" + \
               self.header(method, api_url, *args) + "'"
        return ccmd
    def getDestroyCommand(self, idstr):
        # return the curl command to post a DM
        return self.curlCommand('POST', 'https://api.twitter.com/1.1/statuses/destroy/' + str(idstr) + \
                                ".json") 
    def postDirectMessageCommand(self, screenname, tweet):
        # return the curl command to post a DM
        return self.curlCommand('POST', 'https://api.twitter.com/1.1/direct_messages/new.json', \
                                tweet, screenname)        
    def getDirectMessageCommand(self):
        # return the curl command to get DMs
        return self.curlCommand('GET', 'https://api.twitter.com/1.1/direct_messages.json')
    def postTweetCommand(self, tweet):
        # return the curl command to post a tweet
        return self.curlCommand('POST', 'https://api.twitter.com/1.1/statuses/update.json', tweet)
    def getTweetsCommand(self):
        # return the curl command to retrieve tweets from timeline
        return self.curlCommand('GET', 'https://api.twitter.com/1.1/statuses/user_timeline.json')
    def getHomeCommand(self):
        # return the curl command to retrieve tweets from home timeline
        return self.curlCommand('GET', 'https://api.twitter.com/1.1/statuses/home_timeline.json')
    def getTweetsJson(self):
        # return a json structure containing the last tweets from timeline
        return json.loads(self.getTweets())
    def getHomeJson(self):
        # return a json structure containing the last tweets from home
        return json.loads(self.getHome())
    def getTweets(self):
        # return a string containing the last tweets from timeline, fromatted as a json structure
        return self.do('user_timeline')
    def getHome(self):
        # return a string containing the last tweets from timeline, fromatted as a json structure
        return self.do('home_timeline')
    def destroy(self, idstr):
        # destroy a given status
        return self.do('destroy', idstr)
    def do(self, what, *args):
        path = ''
        if (what == 'user_timeline'):
            path = subprocess.check_output(["which",  "curl"]).rstrip('\n') + ' ' + \
                   self.getTweetsCommand().strip('curl ')
        elif (what == 'update'):
            path = subprocess.check_output(["which",  "curl"]).rstrip('\n') + ' ' + \
                   self.postTweetCommand(args[0]).strip('curl ')            
        elif (what == 'direct_messages') and (len(args) > 0):
            path = subprocess.check_output(["which",  "curl"]).rstrip('\n') + ' ' + \
                   self.postDirectMessageCommand(args[0], args[1]).strip('curl ')            
        elif (what == 'direct_messages') and (len(args) == 0):
            path = subprocess.check_output(["which",  "curl"]).rstrip('\n') + ' ' + \
                   self.getDirectMessageCommand().strip('curl ')            
        elif (what == 'destroy'):
            path = subprocess.check_output(["which",  "curl"]).rstrip('\n') + ' ' + \
                   self.getDestroyCommand(args[0]).strip('curl ')            
        elif (what == 'home_timeline'):
            path = subprocess.check_output(["which",  "curl"]).rstrip('\n') + ' ' + \
                   self.getHomeCommand().strip('curl ')            
        args = shlex.split(path)
        fnull = open(os.devnull, 'w')
        result = subprocess.check_output(args, stderr=fnull)
        return result 
    def postTweet(self, tweet):
        return self.do('update', tweet)
    def postDirectMessage(self, screen_name, tweet):
        return self.do('direct_messages', screen_name, tweet)
    def getDirectMessages(self):
        return self.do('direct_messages')
    def getTweetText(self, *args):
        # get tweets from the timeline. Optionally pass the user screen_name, a formatted timestamp
        # and a text to select only tweets from screen_name, posted after timestamp and containing 
        # the text in their body
        from_user = ''
        match_with = ''
        since_epoch = 0
        if (len(args) >= 1):
            from_user = args[0]
        if (len(args) >= 2):
            since = time.strptime(args[1], "%a %b %d %H:%M:%S +0000 %Y")
            since_epoch = time.mktime(since)
        if (len(args) >= 3):
            match_with = args[2]
        obj = self.getHomeJson()
        lst = []
        for o in obj:
            text = o['text']
            user = o['user']['screen_name']
            timestamp = o['created_at']
            msgid = o['id_str']
            lst.append(user + ":" + text + ":" + timestamp + ":" + msgid)
            pop = 0
            if (from_user != '') and (from_user != user):
                pop = 1
            if (since_epoch > 0):
                time_epoch = time.mktime(time.strptime(timestamp, "%a %b %d %H:%M:%S +0000 %Y"))
                if (time_epoch < since_epoch):
                    pop = 1
            if (match_with != ''):
                if not match_with in text:
                    pop = 1
            if (pop > 0):
                lst.pop()
        return lst

oauth = OAuth()
# the following command retrieves tweets from your timeline whose author is 'organtin'
#pprint.pprint(oauth.getTweetText('organtin'))
# the following command post a tweet
#pprint.pprint(oauth.postTweet('ciao ciao'))
# the following command post a direct message
#pprint.pprint(oauth.postDirectMessage('fontebuono104', 'Hai ricevuto questo messaggio?'))
# the following command get your direct messages
#pprint.pprint(oauth.getDirectMessages())
# the following command destroy a status with a given id
#print oauth.destroy(560446427726626817)
# the following command retrieves the user home
#pprint.pprint(oauth.getHome())

# TODO
# add since_id
