#/bin/bash

############################################################################                                   
#                                                                                                              
#    getTweet.sh - a shell script to get your twitter timeline                                                 
#                  copyright (c) by giovanni.organtini@roma1.infn.it 2015          
#                                                                                                              
#    This program is free software: you can redistribute it and/or modify                                      
#    it under the terms of the GNU General Public License as published by                                      
#    the Free Software Foundation, either version 3 of the License, or                                         
#    (at your option) any later version.                                                                       
#                                                                                                              
#    This program is distributed in the hope that it will be useful,                                           
#    but WITHOUT ANY WARRANTY; without even the implied warranty of                                            
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                             
#    GNU General Public License for more details.                                                              
#                                                                                                              
#    You should have received a copy of the GNU General Public License                                         
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.                                     
#                                                                                                              
############################################################################                                   

rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02X' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"    # You can either set a return variable (FASTER) 
  REPLY="${encoded}"   #+or echo the result (EASIER)... or both... :p
}



# the API URL
API_URL=https://api.twitter.com/1.1/statuses/user_timeline.json

# timestamp is the current time in Unix epoch
TIMESTAMP=`date +%s`

NONCE=${TIMESTAMP}
NONCE=`echo -n ${NONCE} | openssl base64`

# these are get from apps.twitter.com/app
source .secrets

# urlencode keys
CONSUMERKEY=$(rawurlencode ${CONSUMERKEY})
NONCE=$(rawurlencode ${NONCE})
TIMESTAMP=$(rawurlencode ${TIMESTAMP})
ACCESSTOKEN=$(rawurlencode ${ACCESSTOKEN})

# building the parameter string
PARAMETER_STRING='oauth_consumer_key='${CONSUMERKEY}'&oauth_nonce='${NONCE}'&oauth_signature_method=HMAC-SHA1&oauth_timestamp='${TIMESTAMP}'&oauth_token='${ACCESSTOKEN}'&oauth_version=1.0'

PARAMETER_STRING='GET&'$(rawurlencode ${API_URL})'&'$(rawurlencode ${PARAMETER_STRING})

# building the signature
SIGNATUREBASESTR=${PARAMETER_STRING}

# and the key
SIGNINGKEY=$(rawurlencode ${CONSUMERSECRET})'&'$(rawurlencode ${ACCESSTOKENSECRET})





# getting the encoded signature
ENCSIGNATURE=`echo -n ${SIGNATUREBASESTR} | openssl sha1 -hmac ${SIGNINGKEY} -binary`

ENCSIGNATURE=`echo -n ${ENCSIGNATURE} | openssl base64`
ENCSIGNATURE=$(rawurlencode ${ENCSIGNATURE})

# build the header
HEADER='Authorization: OAuth oauth_consumer_key="'${CONSUMERKEY}'", oauth_nonce="'${NONCE}'", oauth_signature_method="HMAC-SHA1", oauth_token="'${ACCESSTOKEN}'", oauth_timestamp="'${TIMESTAMP}'", oauth_version="1.0", oauth_signature="'${ENCSIGNATURE}'"'

HEADER="'"${HEADER}"'"

CMD='curl --get '${API_URL}' --header '${HEADER}

eval ${CMD}


